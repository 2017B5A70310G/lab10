import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class STTestMain extends JFrame {
    STDrawingArea drawingArea = new STDrawingArea();
    public STTestMain()
    {
        //JFrame settings
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Spelling Trainer");
        setResizable(false);
        setVisible(true);
        requestFocusInWindow();


        //Panel of buttons
        JPanel buttonContainer = new JPanel();
        JButton btnRedPen = new JButton("Red Pen");
        JButton btnGreenPen = new JButton("Green Pen");
        JButton btnBluePen = new JButton("Blue Pen");
        JButton btnClear = new JButton("Clear");
        buttonContainer.add(btnRedPen);
        buttonContainer.add(btnGreenPen);
        buttonContainer.add(btnBluePen);
        buttonContainer.add(btnClear);
        //Drawing Area instantiation


        //Adding things to JFrame
        getContentPane().add(drawingArea);
        getContentPane().add(buttonContainer,BorderLayout.PAGE_END);
        pack();


        //button listener
        btnRedPen.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                
                if (btnRedPen.getForeground() == Color.RED) {
                	btnRedPen.setForeground(Color.BLACK);
                	drawingArea.setCurrentColor(Color.BLACK);
                	return;
                }
               
               	btnRedPen.setForeground(Color.RED);
               	btnGreenPen.setForeground(Color.BLACK);
               	btnBluePen.setForeground(Color.BLACK);
                drawingArea.setCurrentColor(Color.RED);
            }
        });

        btnGreenPen.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                
                if (btnGreenPen.getForeground() == Color.GREEN) {
                	btnGreenPen.setForeground(Color.BLACK);
                	drawingArea.setCurrentColor(Color.BLACK);
                	return;
                }
                
                btnRedPen.setForeground(Color.BLACK);
               	btnGreenPen.setForeground(Color.GREEN);
               	btnBluePen.setForeground(Color.BLACK);
                drawingArea.setCurrentColor(Color.GREEN);
            }
        });
        
        btnBluePen.addActionListener(new ActionListener() {
        	
        	public void actionPerformed(ActionEvent e) {
        	
        		if (btnBluePen.getForeground() == Color.BLUE) {
                		btnBluePen.setForeground(Color.BLACK);
                		drawingArea.setCurrentColor(Color.BLACK);
                		return;
                	}
        	
        		btnRedPen.setForeground(Color.BLACK);
		       	btnGreenPen.setForeground(Color.BLACK);
		       	btnBluePen.setForeground(Color.BLUE);
		        drawingArea.setCurrentColor(Color.BLUE);
        	}
        });

        btnClear.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                drawingArea.clearDrawings();
                btnRedPen.setForeground(Color.BLACK);
                btnGreenPen.setForeground(Color.BLACK);
                btnBluePen.setForeground(Color.BLACK);
                drawingArea.setCurrentColor(Color.BLACK);
            }
        });
        
        addKeyListener(new KeyListener() {
   		public void keyTyped(KeyEvent e) {
		}
	
		public void keyPressed(KeyEvent e) {
			System.out.println(e.getKeyChar());
			
			if (e.getKeyChar() == 'r') {
				Color drawingAreaColor = drawingArea.getCurrentColor();
                
				if (btnRedPen.getForeground() == Color.RED) {
					btnRedPen.setForeground(Color.BLACK);
					drawingArea.setCurrentColor(new Color(0, drawingAreaColor.getGreen(), drawingAreaColor.getBlue()));
				}
				else {
					btnRedPen.setForeground(Color.RED);
					drawingArea.setCurrentColor(new Color(255, drawingAreaColor.getGreen(), drawingAreaColor.getBlue()));
				}
			}
			
			if (e.getKeyChar() == 'g') {
				Color drawingAreaColor = drawingArea.getCurrentColor();
                
				if (btnGreenPen.getForeground() == Color.GREEN) {
					btnGreenPen.setForeground(Color.BLACK);
					drawingArea.setCurrentColor(new Color(drawingAreaColor.getRed(), 0, drawingAreaColor.getBlue()));
				}
				else {
					btnGreenPen.setForeground(Color.GREEN);
					drawingArea.setCurrentColor(new Color(drawingAreaColor.getRed(), 255, drawingAreaColor.getBlue()));
				}
			}
			
			if (e.getKeyChar() == 'b') {
				Color drawingAreaColor = drawingArea.getCurrentColor();
                
				if (btnBluePen.getForeground() == Color.BLUE) {
					btnBluePen.setForeground(Color.BLACK);
					drawingArea.setCurrentColor(new Color(drawingAreaColor.getRed(), drawingAreaColor.getGreen(), 0));
				}
				else {
					btnBluePen.setForeground(Color.BLUE);
					drawingArea.setCurrentColor(new Color(drawingAreaColor.getRed(), drawingAreaColor.getGreen(), 255));
				}
			}
			
			if (e.getKeyChar() == 'c') {
				drawingArea.clearDrawings();
				btnRedPen.setForeground(Color.BLACK);
				btnGreenPen.setForeground(Color.BLACK);
				btnBluePen.setForeground(Color.BLACK);
				drawingArea.setCurrentColor(Color.BLACK);
			}
		}
		
		public void keyReleased(KeyEvent e) {
		}
   	});
        
    }
    

    public static void main(String args[])
    {
        STTestMain test = new STTestMain();
    }

}

